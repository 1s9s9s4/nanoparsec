module NanoParsec where
    import Data.Char
    import Control.Applicative
    import Control.Monad

    -- a is the type of the abstract syntax tree
    data Parser a = Parser {
        parse :: String -> [(a,String)]
    } 
    
    -- typeclass instances of Parser. It's a monad
    instance Functor Parser where
        fmap f (Parser p) = Parser (\s -> [ (f a,t) | (a,t) <- p s ])

    instance Applicative Parser where
        pure a = Parser (\s -> [(a,s)])
        (Parser func) <*> (Parser a) = Parser (\s -> 
                                            [(f a,t) | (f,s1) <- func s, (a,t) <- a s1] )
    
    instance Monad Parser where
        return = pure
        (Parser a) >>= f = Parser (\s ->
                                [ (c,t) | (b,s1) <- a s, (c,t) <- parse (f b) s1 ])



    {- >>= can be equally defined using concatMap
        (Parser a) >>= f = Parser (\s -> concatMap (\(a,s') -> (f a, s'))  (parse a s)) 
    
    -} 
    
    --This parser has a zero value corresponding to failure i.e. (\s -> [])
    -- Alternative Typeclass encodes trying two different parsers and choosing one
    -- MonadPlus Typeclass encodes combining the work of two parsers

    instance Alternative Parser where
        empty = mzero
        a <|> b = Parser $ (\s ->
                                case parse a s of
                                    mzero -> parse b s
                                    res -> res
                            )    
    instance MonadPlus Parser where
        mzero = Parser $ \s -> []
        mplus (Parser a) (Parser b) = Parser (\s -> (a s) ++ (b s))

    
    runParser :: Parser a -> String -> Maybe a
    runParser parser code = 
        case parse parser code of
            [(result,[])] -> Just result
            [(_,remainingString)] -> Nothing
            _ -> Nothing

    item :: Parser Char
    item = Parser $ \s ->
                        case s of 
                            [] -> []
                            (c:cs) -> [(c,cs)]

    satisfyPredicate :: (Char -> Bool) -> Parser Char
    satisfyPredicate p = item >>= \character -> 
                                        case p character of 
                                            True -> return character
                                            False -> mzero

    